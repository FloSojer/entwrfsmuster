package Builder;

import Builder.builder.HouseBuilder;
import Builder.builder.HouseManualBuilder;
import Builder.house.House;
import Builder.house.Manual;

public class Builder_Client extends Director {
    public Builder_Client(){
        Director director = new Director();

        HouseBuilder houseBuilder = new HouseBuilder();
        director.constructApartment(houseBuilder);

        House house = houseBuilder.getResult();
        System.out.println("house built "+ house.gethTpye());

        HouseManualBuilder houseManualBuilder = new HouseManualBuilder();
        director.constructApartment(houseManualBuilder);
        Manual manual = houseManualBuilder.getResult();
        System.out.println("House Manual Built: " + manual.print());
    }
}
