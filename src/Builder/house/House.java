package Builder.house;

import Builder.components.GardenType;

public class House {
    private final HouseType hTpye;
    private final int rooms;
    private final GardenType gType;
    private final NeighbourHood nbhood;
    private final int sqmeters;
    private double electricity;

    public House(HouseType houseType, int rooms, GardenType gType, NeighbourHood nbhood,int sqmeters){
        this.hTpye = houseType;
        this.rooms = rooms;
        this.gType = gType;
        this.nbhood = nbhood;
        this.sqmeters = sqmeters;
    }

    public HouseType gethTpye() {
        return hTpye;
    }

    public int getRooms() {
        return rooms;
    }

    public GardenType getgType() {
        return gType;
    }

    public NeighbourHood getNbhood() {
        return nbhood;
    }

    public int getSqmeters() {
        return sqmeters;
    }

    public double getElectricity() {
        return electricity;
    }

    public void setElectricity(double electricity) {
        this.electricity = electricity;
    }
}
