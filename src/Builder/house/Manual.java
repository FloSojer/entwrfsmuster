package Builder.house;

import Builder.components.GardenType;

public class Manual {
    private HouseType hTpye;
    private int rooms;
    private GardenType gType;
    private NeighbourHood nbhood;
    private int sqmeters;

    public Manual(HouseType houseType, int rooms, GardenType gType, NeighbourHood nbhood,int sqmeters){
        this.hTpye = houseType;
        this.rooms = rooms;
        this.gType = gType;
        this.nbhood = nbhood;
        this.sqmeters = sqmeters;
    }

    public String print(){
        String info = "";
        info += "Type of House: " + hTpye + "\n";
        info += "Number of Rooms: " + rooms + "\n";
        info += "Type of Garden: " + gType + "\n";
        info += "Type of Neighbourhood: " + nbhood + "\n";
        info += "Squaremeters: " + sqmeters + "\n";

        return info;
    }
}
