package Builder;

import Builder.builder.Builder;
import Builder.components.GardenType;
import Builder.house.HouseType;
import Builder.house.NeighbourHood;

public class Director {
    public void constructBungallowForPoor(Builder builder){
        builder.setHouseType(HouseType.BUNGALLOW);
        builder.setGardenType(GardenType.SMALL);
        builder.setNeighbourHood(NeighbourHood.BAD);
        builder.setRooms(4);
        builder.setSqMeters(65);
    }
    public void constructBungallowForRich(Builder builder){
        builder.setHouseType(HouseType.BUNGALLOW);
        builder.setGardenType(GardenType.BIG);
        builder.setNeighbourHood(NeighbourHood.FAMILYFRIENDLY);
        builder.setRooms(8);
        builder.setSqMeters(180);
    }
    public void constructVillaOK(Builder builder){
        builder.setHouseType(HouseType.VILLA);
        builder.setGardenType(GardenType.BIG);
        builder.setNeighbourHood(NeighbourHood.GOOD);
        builder.setRooms(7);
        builder.setSqMeters(200);
    }
    public void constructVillaGood(Builder builder){
        builder.setHouseType(HouseType.VILLA);
        builder.setGardenType(GardenType.BIG_WITH_POOL);
        builder.setNeighbourHood(NeighbourHood.FAMILYFRIENDLY);
        builder.setRooms(9);
        builder.setSqMeters(230);
    }

    public void constructApartment(Builder builder){
        builder.setHouseType(HouseType.APARTMENT);
        builder.setGardenType(GardenType.NONE);
        builder.setNeighbourHood(NeighbourHood.INDUSTRIAL);
        builder.setRooms(3);
        builder.setSqMeters(45);
    }

}
