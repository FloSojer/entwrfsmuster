package Builder.components;

public enum GardenType {
    NONE, SMALL, BIG_WITH_POOL, BIG, LABYRINTH
}
