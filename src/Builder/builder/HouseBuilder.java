package Builder.builder;

import Builder.builder.Builder;
import Builder.components.GardenType;
import Builder.house.House;
import Builder.house.HouseType;
import Builder.house.NeighbourHood;

public class HouseBuilder implements Builder {
   private HouseType hTpye;
   private int rooms;
   private GardenType gType;
   private NeighbourHood nbhood;
   private int sqmeters;

    @Override
    public void setHouseType(HouseType hType) {
        this.hTpye = hType;
    }

    @Override
    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    @Override
    public void setGardenType(GardenType gType) {
        this.gType = gType;
    }

    @Override
    public void setNeighbourHood(NeighbourHood nbhood) {
        this.nbhood = nbhood;
    }
    @Override
    public void setSqMeters(int sqmeters){
        this.sqmeters = sqmeters;
    }

    public House getResult(){
        return new House(this.hTpye, this.rooms, this.gType, this.nbhood, this.sqmeters);
    }
}
