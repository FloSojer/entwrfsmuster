package Builder.builder;

import Builder.builder.Builder;
import Builder.components.GardenType;
import Builder.house.HouseType;
import Builder.house.Manual;
import Builder.house.NeighbourHood;

public class HouseManualBuilder implements Builder {
    private HouseType hTpye;
    private int rooms;
    private GardenType gType;
    private NeighbourHood nbhood;
    private int sqmeters;
    @Override
    public void setHouseType(HouseType hType) {
        this.hTpye = hType;
    }

    @Override
    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    @Override
    public void setSqMeters(int sqMeters) {
        this.sqmeters = sqMeters;
    }

    @Override
    public void setNeighbourHood(NeighbourHood nbhood) {
        this.nbhood = nbhood;
    }

    @Override
    public void setGardenType(GardenType gType) {
        this.gType = gType;
    }

    public Manual getResult(){
        return new Manual(this.hTpye, this.rooms, this.gType, this.nbhood, this.sqmeters);
    }
}
