package Builder.builder;

import Builder.components.GardenType;
import Builder.house.HouseType;
import Builder.house.NeighbourHood;

public interface Builder {
    void setHouseType(HouseType hType);
    void setRooms(int rooms);
    void setGardenType(GardenType gType);
   void  setNeighbourHood(NeighbourHood nbhood);
   void setSqMeters(int sqMeters);
}

