package com.company.entwurfsmuster;

import Adapter.Adapter_pattern;

public class Main {

    public static void main(String[] args) {
	// write your code her
        //Beispiel zu Template Hook
        //Poster poster = new Poster();

        //Beispiel zu Chain Of Responsibility
        //Login fuer admin: admin@example.com pw: amdin_pass
        //Login fuer user: user@example.com pw: user_pass
       /* try {
            UserChecker userChecker = new UserChecker();
        } catch (IOException e) {
            e.printStackTrace();
        } */

        //Beispiel zu Observer
        //Observer obs = new Observer();

        //Beispiel zu Builder
       // Builder_Client builder_client = new Builder_Client();

        //Beispiel zu Adapter
        Adapter_pattern adapter_pattern = new Adapter_pattern();





    }
}
