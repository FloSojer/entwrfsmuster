package Decorator.decor;

public interface DataSource {
    void writeData(String data);
    String readData();
}
