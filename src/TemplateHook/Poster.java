package TemplateHook;

import TemplateHook.networks.Facebook;
import TemplateHook.networks.Instagram;
import TemplateHook.networks.Network;

import java.util.Scanner;

public class Poster {
    public Poster(){
        Scanner scan = new Scanner(System.in);
        Network network = null;
        String username;
        String pw;
        boolean gesendet;
        System.out.println("Wo moechten Sie sich einloggen? ");
        System.out.println("1) Facebook");
        System.out.println("2) Instagram");
        int eingabeZahl = scan.nextInt();

        if(eingabeZahl == 1){
            System.out.println("Bitte geben Sie den Username ein: ");
            username = scan.next();
            System.out.println("Bitte geben Sie ihr pw ein:");
            pw = scan.next();
            network = new Facebook(username, pw);
        }
        else if(eingabeZahl == 2){
            System.out.println("Bitte geben Sie den Username ein: ");
            username = scan.next();
            System.out.println("Bitte geben Sie ihr pw ein:");
            pw = scan.next();
            network = new Instagram(username, pw);
        }
        else{
            System.out.println("Eingabe war falsch");
        }

        System.out.println("Was moechten Sie posten? ");
        String daten = scan.next();
        network.post(daten);
    }
}
