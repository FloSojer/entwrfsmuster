package TemplateHook.networks;

public class Facebook extends Network{
    public Facebook(String username, String password){
        this.username = username;
        this.password =password;
    }
    @Override
    boolean eingelogt(String username, String password) {
        System.out.println("Schauen ob eingegebene Parameter richtig sind!");
        System.out.println("Name: " + this.username);
        System.out.println("Password: ****** ");

        //hier könnten Daten fue Login abgerufen werden
        System.out.println("richtiger Login bei Facebook");

        return true;
    }

    @Override
    boolean datenSenden(String daten) {
        //fuer erweiterung Daten
        boolean isPosted = true;
        if(isPosted){
            System.out.println("Post: " + daten + " wurde geteilt");
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    void logOut(){
        System.out.println("Sie wurden von Facebook ausgelogt!");
    }
}
