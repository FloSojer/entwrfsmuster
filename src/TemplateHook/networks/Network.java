package TemplateHook.networks;

public abstract class Network {
    String username;
    String password;

    public boolean post(String message){
        if(eingelogt(this.username, this.password)){
            boolean datenGesendet = datenSenden(message);
            logOut();
            return datenGesendet;
        }

        return false;
    }

    abstract boolean eingelogt(String username, String password);
    abstract boolean datenSenden(String daten);
    abstract void logOut();
}
