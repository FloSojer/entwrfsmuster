package ChainOfResponsibility.middleware;

import ChainOfResponsibility.middleware.Middleware;

public class RoleChekMiddleWare extends Middleware {
    String[] adminEmails = {"admin@example.com", "flsojer@tsn.at"};
    public boolean check(String email, String password) {
        if (email.equals(adminEmails[0]) || email.equals(adminEmails[1])) {
            System.out.println("Hello, admin!");
            return true;
        }
        System.out.println("Hello, user!");
        return checkNext(email, password);
    }
}
