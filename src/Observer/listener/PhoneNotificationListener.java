package Observer.listener;

import Observer.listener.EventListener;
import Observer.editor.Editor;
import Observer.listener.EmailNotificationListener;
import Observer.listener.LogOpenListener;
import Observer.listener.PhoneNotificationListener;

import java.io.File;

public class PhoneNotificationListener implements EventListener {
    private String phoneNumber;
    public PhoneNotificationListener(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void update(String eventType, File file) {
        System.out.println("PhoneNumber "+ phoneNumber +  "Someone has performed " + eventType + " operation with the following file: " + file.getName());
    }
}
