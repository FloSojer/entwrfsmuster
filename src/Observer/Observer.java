package Observer;

import Observer.editor.Editor;
import Observer.listener.EmailNotificationListener;
import Observer.listener.LogOpenListener;
import Observer.listener.PhoneNotificationListener;

public class Observer {
    public Observer(){
        Editor editor = new Editor();
        editor.events.subscribe("open", new LogOpenListener("/path/to/log/file.txt"));
        editor.events.subscribe("save", new EmailNotificationListener("admin@example.com"));
        editor.events.subscribe("save", new PhoneNotificationListener("066412345678"));


        try {
            editor.openFile("test.txt");
            editor.saveFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
