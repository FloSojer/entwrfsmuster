package Observer.publisher;

import java.io.File;
import java.util.*;
import Observer.editor.Editor;
import Observer.listener.EmailNotificationListener;
import Observer.listener.LogOpenListener;
import Observer.listener.PhoneNotificationListener;
import Observer.listener.EventListener;

public class EventManager {
    Map<String, List<EventListener>> listeners = new HashMap<>();
    public EventManager(String... operations) {
        for (String operation : operations) {
            this.listeners.put(operation, new ArrayList<>());
        }
    }

    public void subscribe(String eventType, EventListener listener) {
        List<EventListener> users = listeners.get(eventType);
        users.add(listener);
    }

    public void unsubscribe(String eventType, EventListener listener) {
        List<EventListener> users = listeners.get(eventType);
        users.remove(listener);
    }

    public void notify(String eventType, File file) {
        List<EventListener> users = listeners.get(eventType);
        for (EventListener listener : users) {
            listener.update(eventType, file);
        }
    }

}
