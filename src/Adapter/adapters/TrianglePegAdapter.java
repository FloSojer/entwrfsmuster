package Adapter.adapters;

import Adapter.round.RoundPeg;
import Adapter.triangle.Triangle;

public class TrianglePegAdapter extends RoundPeg {
    private Triangle triangle;

    public TrianglePegAdapter(Triangle triangle){
        this.triangle = triangle;
    }

    @Override
    public double getRadius() {
        double radius;
        radius = Math.sqrt(3)/3*triangle.getSide();
        return radius;
    }
}
