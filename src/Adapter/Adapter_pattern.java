package Adapter;

import Adapter.adapters.SquarePegAdapter;
import Adapter.adapters.TrianglePegAdapter;
import Adapter.round.RoundHole;
import Adapter.round.RoundPeg;
import Adapter.square.SquarePeg;
import Adapter.triangle.Triangle;

import java.sql.SQLOutput;

/*
Entspricht den Client Code
 */
public class Adapter_pattern {
    public Adapter_pattern(){

        double radius = 5;
        RoundHole hole = new RoundHole(radius);
        RoundPeg rpeg = new RoundPeg(radius);

        if(hole.fits(rpeg)){
            System.out.println("Round peg radius 5 fits into round Hole with r5");
        }

        double laengeKlein = 3;
        double laengeeGross = 10;

        SquarePeg smallSqPeg = new SquarePeg(laengeKlein);
        SquarePeg largeSqPeg = new SquarePeg(laengeeGross);

        SquarePegAdapter smallSqPegAdapter = new SquarePegAdapter(smallSqPeg);
        SquarePegAdapter largeSqPegAdapter = new SquarePegAdapter(largeSqPeg);

        if(hole.fits(smallSqPegAdapter)){
            System.out.println("Sqaure peg width "+laengeKlein+" fits round hole radius: " + radius);
        }
        else{
            System.out.println("Sqaure peg width "+laengeKlein+" fits NOT in round hole radius: " + radius);

        }
        if(hole.fits(largeSqPegAdapter)){
            System.out.println("Sqaure peg width "+laengeeGross +" fits round hole radius: " + radius);
        }
        else {
            System.out.println("Sqaure peg width "+laengeeGross +" fits NOT round hole radius: " + radius);
        }

        double seitenlaengeKlein = 8.66025;
        double seitenlaengeGross = 10;
        Triangle smlTriangle = new Triangle(seitenlaengeKlein);
        Triangle bigTriangle = new Triangle(seitenlaengeGross);
        TrianglePegAdapter smlTrianglePegAdapter = new TrianglePegAdapter(smlTriangle);
        TrianglePegAdapter bigTrianglePegAdapter = new TrianglePegAdapter(bigTriangle);
        if(hole.fits(smlTrianglePegAdapter)){
            System.out.println("Kleines Dreieck mit der Seitenlaenge " + seitenlaengeKlein + " passt in Kreis Radius: " + radius );
        }
        else{
            System.out.println("Kleines Dreieck mit der Seitenlaenge " + seitenlaengeKlein + " passt NICHT Kreis Radius: " + radius );

        }
        if(hole.fits(bigTrianglePegAdapter)){
            System.out.println("Grosses Dreieck mit der Seitenlaenge " + seitenlaengeGross + " passt in Kreis Radius: " + radius );
        }
        else{
            System.out.println("Grosses Dreieck mit der Seitenlaenge " + seitenlaengeGross + " passt NICHT Kreis Radius: " + radius );

        }


        Triangle triangle = new Triangle(1);
        System.out.println("Flaechen Inhalt Dreieck " + triangle.getTriangle());

    }
}
