package Adapter.triangle;

public class Triangle {
    private double side;

    public Triangle(double side){
        this.side = side;
    }

    public double getSide(){
        return this.side;
    }

    public double getTriangle(){
        double result;
        result = Math.pow(this.side, 2)/4*Math.sqrt(3);
        return result;
    }
}
